sudo apt-get update
sudo apt-get -y install gfortran
sudo apt-get -y install build-essential

sudo apt-get -y install libreadline6 libreadline6-dev
sudo apt-get -y install xorg-dev
sudo apt-get -y install openjdk-7-jdk

gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E084DAB9
gpg -a --export E084DAB9 | sudo apt-key add -
sudo add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu trusty/'
sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get -y install r-base-dev
sudo apt-get -y install libssl-dev libffi-dev
sudo apt-get -y install libtorque2-dev libtorque2
sudo apt-get -y install pbs-drmaa-dev pbs-drmaa1

sudo apt-get -y install build-essential checkinstall
sudo apt-get -y install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev

wget https://sourceforge.net/projects/pbspro-drmaa/files/pbs-drmaa/1.0/pbs-drmaa-1.0.19.tar.gz/download -O /home/vagrant/pbs-drmaa-1.0.19.tar.gz
cd /home/vagrant
tar -xzvf pbs-drmaa-1.0.19.tar.gz && cd pbs-drmaa-1.0.19
./configure
make && make install
echo "DRMAA_LIBRARY_PATH=/usr/lib/libdrmaa.so" >> /home/vagrant/.profile

sudo apt-get -y install environment-modules
cd /home/vagrant
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip && unzip fastqc_v0.11.5.zip
chmod 755 FastQC/fastqc

mkdir -p /usr/share/modules/modulefiles/apps/

cat > /usr/share/modules/modulefiles/apps/fastqc-0.11.5 <<EOF
#%Module1.0#####################################################################
##
## modules apps/fastqc-0.11.5
##
## modulefiles/apps/fastqc-0.11.5
##
proc ModulesHelp { } {
        global version modroot

        puts stderr "apps/fastqc-0.11.5 - sets the Environment for fastqc in my home directory"
}

module-whatis   "Sets the environment for using fastqc"

# for Tcl script use only
set     topdir          /home/vagrant/FastQC
set     version         0.11.5

prepend-path    PATH    \$topdir/
EOF

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /home/vagrant/Miniconda3-latest-Linux-x86_64.sh
bash /home/vagrant/Miniconda3-latest-Linux-x86_64.sh -b -p /home/vagrant/miniconda3
sudo chown -R ubuntu /home/vagrant/miniconda3
sudo chgrp -R ubuntu /home/vagrant/miniconda3
echo "export PATH='/home/vagrant/miniconda3/bin:$PATH'" >> /home/vagrant/.profile

# default shell to bash
sudo rm /bin/sh
sudo ln -s /bin/bash /bin/sh
cp /vagrant/myjob.pbs /home/vagrant

# Cleanup
rm -rf /home/vagrant/pbs-drmaa-1.0.19.tar.gz /home/vagrant/fastqc_v0.11.5.zip

cd /home/vagrant
VERSION=2.3.1
wget https://github.com/singularityware/singularity/releases/download/$VERSION/singularity-$VERSION.tar.gz
tar xzvf singularity-$VERSION.tar.gz
cd singularity-$VERSION
./configure --prefix=/usr/
make
sudo make install
cd .. && rm -rf singularity-$VERSION*
